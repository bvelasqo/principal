export class Partida {
    nombrePartida: string;
    tematica: string;
    descripcion: string;
    constructor(np: string, t: string, d: string){
        this.nombrePartida=np;
        this.tematica=t;
        this.descripcion=d;
    }
    getNombrePartida():string{
        return this.nombrePartida;
    }
    getTematica():string{
        return this.tematica;
    }
    getDescripcion():string{
        return this.descripcion;
    }
}