import { RouterModule, Routes } from '@angular/router';
import { Component } from '@angular/core';
import { HomeComponent } from './Components/home/home.component';
import { QuienesSomosComponent } from './Components/quienes-somos/quienes-somos.component';
import { InstruccionesComponent } from './Components/instrucciones/instrucciones.component';
import { MenuUsuarioComponent } from './Components/menu-usuario/menu-usuario.component';
import { PreguntaComponent } from './Components/pregunta/pregunta.component'
import { TerminarPartidaComponent } from './Components/terminar-partida/terminar-partida.component';
import { CreandoPartidaComponent } from './Components/creando-partida/creando-partida.component';
import { PanelAnfitrionComponent } from './Components/panel-anfitrion/panel-anfitrion.component';
import { PuntajesAnfitrionComponent } from './Components/puntajes-anfitrion/puntajes-anfitrion.component';
import { EsperarCrearPartidaComponent } from './Components/esperar-crear-partida/esperar-crear-partida.component';
import { PersonalizadoComponent } from './Components/personalizado/personalizado.component';
import { PartidasCreadasComponent } from './Components/partidas-creadas/partidas-creadas.component';
import { DetallesPartidaComponent } from './Components/detalles-partida/detalles-partida.component';
import { PartidasJugadasComponent } from './Components/partidas-jugadas/partidas-jugadas.component';


const APP_ROUTES: Routes = [
    { path: 'home', component: HomeComponent},
    { path: 'quienes_somos', component: QuienesSomosComponent},
    { path: 'instrucciones', component: InstruccionesComponent},
    { path: 'menu', component: MenuUsuarioComponent},
    { path: 'pregunta', component: PreguntaComponent},
    { path: 'terminar', component: TerminarPartidaComponent},
    { path: 'creando-partida', component: CreandoPartidaComponent},
    { path: 'menu_usuario', component: MenuUsuarioComponent},
    { path: 'panel_anfitrion', component: PanelAnfitrionComponent},
    { path: 'puntajes_anfitrion', component: PuntajesAnfitrionComponent},
    { path: 'esperar_crear', component: EsperarCrearPartidaComponent},
    { path: 'personalizado', component: PersonalizadoComponent},
    { path: 'partidas-creadas', component: PartidasCreadasComponent},
    { path: 'detalles-partida', component: DetallesPartidaComponent},
    { path: 'partidas-jugadas', component: PartidasJugadasComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true});