import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminarPartidaComponent } from './terminar-partida.component';

describe('TerminarPartidaComponent', () => {
  let component: TerminarPartidaComponent;
  let fixture: ComponentFixture<TerminarPartidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TerminarPartidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminarPartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
