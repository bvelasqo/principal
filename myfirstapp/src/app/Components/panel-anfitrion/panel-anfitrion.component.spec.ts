import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelAnfitrionComponent } from './panel-anfitrion.component';

describe('PanelAnfitrionComponent', () => {
  let component: PanelAnfitrionComponent;
  let fixture: ComponentFixture<PanelAnfitrionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelAnfitrionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelAnfitrionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
