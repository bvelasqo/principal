import { Component, OnInit } from '@angular/core';
import{MatDialog} from '@angular/material/dialog';
import { DialogIngresoComponent } from '../dialog-ingreso/dialog-ingreso.component';
import { DialogRegistroComponent } from '../dialog-registro/dialog-registro.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog1:MatDialog,public dialog2:MatDialog) { }

  ngOnInit(): void {
  }

  openDialogRegistro(){
    this.dialog1.open(DialogRegistroComponent);
  }

  openDialogIngreso(){
    this.dialog2.open(DialogIngresoComponent);
  }

}
