import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartidasJugadasComponent } from './partidas-jugadas.component';

describe('PartidasJugadasComponent', () => {
  let component: PartidasJugadasComponent;
  let fixture: ComponentFixture<PartidasJugadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartidasJugadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartidasJugadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
