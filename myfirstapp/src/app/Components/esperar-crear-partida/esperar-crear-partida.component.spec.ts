import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EsperarCrearPartidaComponent } from './esperar-crear-partida.component';

describe('EsperarCrearPartidaComponent', () => {
  let component: EsperarCrearPartidaComponent;
  let fixture: ComponentFixture<EsperarCrearPartidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EsperarCrearPartidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EsperarCrearPartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
