import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-personalizado',
  templateUrl: './personalizado.component.html',
  styleUrls: ['./personalizado.component.css']
})
export class PersonalizadoComponent implements OnInit {

  contador: number;
  constructor(private toastr: ToastrService) {
    this.contador=0;
   }

  ngOnInit(): void {
  }

  notificacion(){
    this.contador=this.contador+1;
    if(this.contador==1){
      this.toastr.success('Continua buscando tus códigos','Pregunta contestada',{
        "closeButton": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
      });
    }else {
      this.toastr.warning('Continua buscando tus códigos','Ya contaste este reto',{
        "closeButton": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
      });
    }
  }
}
