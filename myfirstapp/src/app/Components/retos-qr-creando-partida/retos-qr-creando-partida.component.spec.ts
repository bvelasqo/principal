import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RetosQRCreandoPartidaComponent } from './retos-qr-creando-partida.component';

describe('RetosQRCreandoPartidaComponent', () => {
  let component: RetosQRCreandoPartidaComponent;
  let fixture: ComponentFixture<RetosQRCreandoPartidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RetosQRCreandoPartidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetosQRCreandoPartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
