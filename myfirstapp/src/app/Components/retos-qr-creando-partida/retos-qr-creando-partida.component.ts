import { Component, Input, OnInit } from '@angular/core';
import { Retos } from 'src/app/Models/Retos.model';

@Component({
  selector: 'app-retos-qr-creando-partida',
  templateUrl: './retos-qr-creando-partida.component.html',
  styleUrls: ['./retos-qr-creando-partida.component.css']
})
export class RetosQRCreandoPartidaComponent implements OnInit {

  @Input() reto:Retos;
  constructor() { }

  ngOnInit(): void {
  }

}
