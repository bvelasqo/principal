import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalasCreandoPartidasComponent } from './salas-creando-partidas.component';

describe('SalasCreandoPartidasComponent', () => {
  let component: SalasCreandoPartidasComponent;
  let fixture: ComponentFixture<SalasCreandoPartidasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalasCreandoPartidasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalasCreandoPartidasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
