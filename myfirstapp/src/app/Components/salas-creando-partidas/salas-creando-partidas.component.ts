import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Salas } from 'src/app/Models/salas.model';

@Component({
  selector: 'app-salas-creando-partidas',
  templateUrl: './salas-creando-partidas.component.html',
  styleUrls: ['./salas-creando-partidas.component.css']
})
export class SalasCreandoPartidasComponent implements OnInit {
  @Input() sala : Salas
  constructor() { }

  ngOnInit(): void {
  }

}
