import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Jugadores } from 'src/app/Models/Jugadores.model';
import { Partida } from 'src/app/Models/Partida.model';
import { Retos } from 'src/app/Models/Retos.model';
import { Salas } from 'src/app/Models/salas.model';

@Component({
  selector: 'app-creando-partida',
  templateUrl: './creando-partida.component.html',
  styleUrls: ['./creando-partida.component.css']
})
export class CreandoPartidaComponent implements OnInit {
  salas : Salas[];
  retos : Retos[];
  jugadores : Jugadores[];
  partida: Partida;

  constructor(private toastr: ToastrService) { 
    this.salas=[];
    this.retos=[];
    this.jugadores=[];
    this.partida=new Partida("","","");
  }

  ngOnInit(): void {
  }

  AgregarSala(nombre: string):boolean{
    this.salas.push(new Salas(nombre));
    return false;
  }

  AgregarReto(n: string, s:string , op1: string ="", op2: string="", op3:string="",op4:string=""):boolean{
    this.retos.push(new Retos(n,s,op1,op2,op3,op4));
    this.toastr.success('Guardado','Continua agregando retos o continua con la otra sección',{
      "closeButton": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-bottom-right",
  });
    return false;
  }

  AgregarJugador(ced: string):boolean{
    this.jugadores.push(new Jugadores(ced));
    return false;
  }

  IngresarDatosPrincipales(np: string,t:string,d:string):boolean{
    this.partida=new Partida(np,t,d);
    this.toastr.success('Guardado','Continua con la otra sección',{
      "closeButton": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-bottom-right",
  });
    return false;
  }

  QuitarSala(nombre: string):boolean{
    this.salas.forEach(x => {
      if(x.getNombre()===nombre){
        var i = this.salas.indexOf( x );
        this.salas.splice( i, 1 );
      }
    });
    return false;
  }

  QuitarJugador(ced: string):boolean{
    this.jugadores.forEach(x => {
      if(x.getCedula()===ced){
        var i = this.jugadores.indexOf( x );
        this.jugadores.splice( i, 1 );
      }
    });
    return false;
  }

}

