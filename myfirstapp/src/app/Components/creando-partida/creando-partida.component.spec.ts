import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreandoPartidaComponent } from './creando-partida.component';

describe('CreandoPartidaComponent', () => {
  let component: CreandoPartidaComponent;
  let fixture: ComponentFixture<CreandoPartidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreandoPartidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreandoPartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
