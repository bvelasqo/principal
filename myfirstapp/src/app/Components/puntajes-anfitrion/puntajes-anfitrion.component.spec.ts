import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntajesAnfitrionComponent } from './puntajes-anfitrion.component';

describe('PuntajesAnfitrionComponent', () => {
  let component: PuntajesAnfitrionComponent;
  let fixture: ComponentFixture<PuntajesAnfitrionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PuntajesAnfitrionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntajesAnfitrionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
