import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-ingreso',
  templateUrl: './dialog-ingreso.component.html',
  styleUrls: ['./dialog-ingreso.component.css']
})
export class DialogIngresoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogIngresoComponent>,
  ) { }

  ngOnInit(): void {
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}
