import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogIngresoComponent } from './dialog-ingreso.component';

describe('DialogIngresoComponent', () => {
  let component: DialogIngresoComponent;
  let fixture: ComponentFixture<DialogIngresoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogIngresoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogIngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
