import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartidasCreadasComponent } from './partidas-creadas.component';

describe('PartidasCreadasComponent', () => {
  let component: PartidasCreadasComponent;
  let fixture: ComponentFixture<PartidasCreadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartidasCreadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartidasCreadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
