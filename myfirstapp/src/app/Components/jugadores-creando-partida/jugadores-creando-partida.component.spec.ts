import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadoresCreandoPartidaComponent } from './jugadores-creando-partida.component';

describe('JugadoresCreandoPartidaComponent', () => {
  let component: JugadoresCreandoPartidaComponent;
  let fixture: ComponentFixture<JugadoresCreandoPartidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JugadoresCreandoPartidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadoresCreandoPartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
