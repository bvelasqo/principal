import { Component, Input, OnInit } from '@angular/core';
import { Jugadores } from 'src/app/Models/Jugadores.model';

@Component({
  selector: 'app-jugadores-creando-partida',
  templateUrl: './jugadores-creando-partida.component.html',
  styleUrls: ['./jugadores-creando-partida.component.css']
})
export class JugadoresCreandoPartidaComponent implements OnInit {

  @Input() jugador: Jugadores;
  @Input('idx') position: number;
  constructor() { }
  ngOnInit(): void {
  }

}
