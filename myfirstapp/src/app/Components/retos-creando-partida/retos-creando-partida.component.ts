import { Component, Input, OnInit } from '@angular/core';
import { Retos } from 'src/app/Models/Retos.model';

@Component({
  selector: 'app-retos-creando-partida',
  templateUrl: './retos-creando-partida.component.html',
  styleUrls: ['./retos-creando-partida.component.css']
})
export class RetosCreandoPartidaComponent implements OnInit {

  @Input() reto:Retos;
  constructor() { }

  ngOnInit(): void {
  }

}
