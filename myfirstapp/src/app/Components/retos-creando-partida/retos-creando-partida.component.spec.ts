import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RetosCreandoPartidaComponent } from './retos-creando-partida.component';

describe('RetosCreandoPartidaComponent', () => {
  let component: RetosCreandoPartidaComponent;
  let fixture: ComponentFixture<RetosCreandoPartidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RetosCreandoPartidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetosCreandoPartidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
