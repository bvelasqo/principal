import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Rutas
import { APP_ROUTING } from './app.routes';

// Componentes
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { HomeComponent } from './Components/home/home.component';
import { QuienesSomosComponent } from './Components/quienes-somos/quienes-somos.component';
import { InstruccionesComponent } from './Components/instrucciones/instrucciones.component';
import { MenuUsuarioComponent } from './Components/menu-usuario/menu-usuario.component';
import { PreguntaComponent } from './Components/pregunta/pregunta.component';
import { TerminarPartidaComponent } from './Components/terminar-partida/terminar-partida.component';
import { CreandoPartidaComponent } from './Components/creando-partida/creando-partida.component';
import { PanelAnfitrionComponent } from './Components/panel-anfitrion/panel-anfitrion.component';
import { SalasCreandoPartidasComponent } from './Components/salas-creando-partidas/salas-creando-partidas.component';
import { PuntajesAnfitrionComponent } from './Components/puntajes-anfitrion/puntajes-anfitrion.component';
import { DialogRegistroComponent } from './Components/dialog-registro/dialog-registro.component';
import {MatDialogModule} from '@angular/material/dialog';
import { DialogIngresoComponent } from './Components/dialog-ingreso/dialog-ingreso.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { JugadoresCreandoPartidaComponent } from './Components/jugadores-creando-partida/jugadores-creando-partida.component';
import { RetosCreandoPartidaComponent } from './Components/retos-creando-partida/retos-creando-partida.component';
import { RetosQRCreandoPartidaComponent } from './Components/retos-qr-creando-partida/retos-qr-creando-partida.component';
import { EsperarCrearPartidaComponent } from './Components/esperar-crear-partida/esperar-crear-partida.component';
import { PersonalizadoComponent } from './Components/personalizado/personalizado.component';
import { PartidasCreadasComponent } from './Components/partidas-creadas/partidas-creadas.component';
import { DetallesPartidaComponent } from './Components/detalles-partida/detalles-partida.component';
import { PartidasJugadasComponent } from './Components/partidas-jugadas/partidas-jugadas.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,  
    HomeComponent,
    QuienesSomosComponent,
    InstruccionesComponent,
    MenuUsuarioComponent,
    PreguntaComponent,
    TerminarPartidaComponent,
    CreandoPartidaComponent,
    PanelAnfitrionComponent,
    PuntajesAnfitrionComponent,
    SalasCreandoPartidasComponent,
    PanelAnfitrionComponent,
    DialogRegistroComponent,
    DialogIngresoComponent,
    JugadoresCreandoPartidaComponent,
    RetosCreandoPartidaComponent,
    RetosQRCreandoPartidaComponent,
    EsperarCrearPartidaComponent,
    PersonalizadoComponent,
    EsperarCrearPartidaComponent,
    PartidasCreadasComponent,
    MenuUsuarioComponent,
    DetallesPartidaComponent,
    PartidasJugadasComponent,
  ],
  entryComponents: [DialogRegistroComponent,DialogIngresoComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    APP_ROUTING,
    MatDialogModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }